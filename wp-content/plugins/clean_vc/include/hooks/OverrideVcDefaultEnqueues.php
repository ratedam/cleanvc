<?php

/*
	Remove visual composer default enqueues
*/


namespace sharepress\Cleanvc\app\Services\General\Controllers\Hooks;

 use sharepress\Cleanvc\app\Services\General\Helpers\config;

 class enqueues{


	protected $action = 'wp_enqueue_scripts';
	
	public function __construct(){
		add_action( $this->action, [ $this, 'run' ]);
	}

	public function run(){

		$files_to_ignore = config::get('vc_ignore_enqueue');

		foreach( $files_to_ignore['styles'] as $file_to_ignore ){
			wp_deregister_style( $file_to_ignore );
		}

		foreach( $files_to_ignore['scripts'] as $file_to_ignore ){
			wp_deregister_script('wpb_composer_front_js');
		}
		
	}
}