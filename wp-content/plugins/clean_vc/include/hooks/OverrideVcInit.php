<?php



namespace sharepress\Cleanvc\app\Services\General\Controllers\Hooks;

use sharepress\Cleanvc\app\Services\General\Helpers\config;

class init{


	protected $action = 'wp_loaded';
	
	public function __construct(){
		add_action( $this->action, [ $this, 'run' ]);
	}

	public function run(){
		if( vc_manager() ){
			vc_manager()->setCustomUserShortcodesTemplateDir( config::getViewsDirectory() );
		}
	}
}