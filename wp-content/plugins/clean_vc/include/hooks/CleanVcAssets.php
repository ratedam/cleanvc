<?php

/*
	Remove visual composer default enqueues
*/


namespace sharepress\Cleanvc\app\Services\General\Controllers\Hooks;

use sharepress\Cleanvc\app\Services\General\Helpers\config;

class assets{

    protected $action = 'wp_enqueue_scripts';

    protected $basePath;

    public function __construct(){
        $this->basePath = CLEANVC_BASEDIR . DIRECTORY_SEPARATOR.  'dist' . DIRECTORY_SEPARATOR;
        add_action( $this->action, [ $this, 'run' ]);
    }

    public function run(){

        $files_to_add = config::get('resources');

        $files_to_add = $files_to_add['frontend'];

        $css_files = $files_to_add['css'];

        foreach($css_files as $css_file){
            $file = $this->basePath . 'css' . DIRECTORY_SEPARATOR . $css_file;
            if( file_exists( $file )){
                wp_register_style( 'clean-vc', $file );
            }
        }


        wp_enqueue_style( 'clean-vc ' );


        /*$files_to_ignore = config::get('vc_ignore_enqueue');

        foreach( $files_to_ignore['styles'] as $file_to_ignore ){
            wp_deregister_style( $file_to_ignore );
        }

        foreach( $files_to_ignore['scripts'] as $file_to_ignore ){
            wp_deregister_script('wpb_composer_front_js');
        }*/

    }
}