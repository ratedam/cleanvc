<?php



namespace sharepress\Cleanvc\app\Services\General\Helpers;

class config{

	//private static $config_file = CLEANVC_BASEDIR . '/config/app.php';



	public static function get( $key ){

		return self::readKey( $key );
	}

	public static function getViewsDirectory(){
		$data = include( self::$config_file );
		return $data['views']['directory'];
	}


	/*
		Read a single value from the file
	*/
	private static function readKey( $key ){
		if( ! file_exists( self::$config_file ) ){
			return null;
		}



		$data = include(self::$config_file);

		if( ! isset( $data[$key] ) ){
			return null;
		}

		return $data[$key];
	}
}