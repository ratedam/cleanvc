<?php
/**
 * A configuração de base do WordPress
 *
 * Este ficheiro define os seguintes parâmetros: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, e ABSPATH. Pode obter mais informação
 * visitando {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} no Codex. As definições de MySQL são-lhe fornecidas pelo seu serviço de alojamento.
 *
 * Este ficheiro contém as seguintes configurações:
 *
 * * Configurações de  MySQL
 * * Chaves secretas
 * * Prefixo das tabelas da base de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Definições de MySQL - obtenha estes dados do seu serviço de alojamento** //
/** O nome da base de dados do WordPress */
define('DB_NAME', 'clean_vc');

/** O nome do utilizador de MySQL */
define('DB_USER', 'root');

/** A password do utilizador de MySQL  */
define('DB_PASSWORD', '');

/** O nome do serviddor de  MySQL  */
define('DB_HOST', 'localhost');

/** O "Database Charset" a usar na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O "Database Collate type". Se tem dúvidas não mude. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação.
 *
 * Mude para frases únicas e diferentes!
 * Pode gerar frases automáticamente em {@link https://api.wordpress.org/secret-key/1.1/salt/ Serviço de chaves secretas de WordPress.org}
 * Pode mudar estes valores em qualquer altura para invalidar todos os cookies existentes o que terá como resultado obrigar todos os utilizadores a voltarem a fazer login
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '~iy/GB`(ex46OP(K`=^!xp-]EIpJ%H#;k>3T2IJg9cME4Wl2spS!p*Mb-Zaw{,Rd');
define('SECURE_AUTH_KEY',  'S+_gQ|0.#jBv&sovKSMWqY0[> !E){B<ynU8C*>AI3h2M!Gw-;Enm2OE4v/PcNx$');
define('LOGGED_IN_KEY',    '@[BnJ}8wV8s<}4H(r=V!2)m.b=2aUc(G1D&9-~=k[aLkD-gpQKsJ]z.fz&K@s:eo');
define('NONCE_KEY',        'XH_58N.)?(8%s<s}P:.{+)c(M,:xoGyh[gm%O-]d`aSK/Ar~Q7ny.v%t2 k?J4*E');
define('AUTH_SALT',        '4Z%@wIiO,Yz#{wMW5~&=mHPp6)|w]Pu3/@pSP_R:)029YK3T|jpdr.3P}Czj!PwO');
define('SECURE_AUTH_SALT', '^G7WnW-^b|cNjRMn(G@F A4Yeyb]W?I|E$.Q>r-jNg|KSMWg+Rr{xYM}?fw!o`*x');
define('LOGGED_IN_SALT',   ')dC=uYTQ<e_}v$Kzc+r1000+W#OW#*7QJ,Axt(WT/r29vXC>@GD]EYb@DjT+)DKG');
define('NONCE_SALT',       '<UYvl8ES#AQA)S-7j%n/}1uQnK>TI}T>6f1NIc*KtmWTk&LAV?fF>!D;q^Sm+|Mn');

/**#@-*/

/**
 * Prefixo das tabelas de WordPress.
 *
 * Pode suportar múltiplas instalações numa só base de dados, ao dar a cada
 * instalação um prefixo único. Só algarismos, letras e underscores, por favor!
 */
$table_prefix  = 'wp_';

/**
 * Para developers: WordPress em modo debugging.
 *
 * Mude isto para true para mostrar avisos enquanto estiver a testar.
 * É vivamente recomendado aos autores de temas e plugins usarem WP_DEBUG
 * no seu ambiente de desenvolvimento.
 *
 * Para mais informações sobre outras constantes que pode usar para debugging,
 * visite o Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* E é tudo. Pare de editar! */

/** Caminho absoluto para a pasta do WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Define as variáveis do WordPress e ficheiros a incluir. */
require_once(ABSPATH . 'wp-settings.php');
